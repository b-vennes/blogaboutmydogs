# All About My Dogs

My dogs are a handful...this one is my favorite:

## Atticus

![atticus](/images/atticus.resizedagain.jpg)

His name is Atticus and he is almost 2 years old. He loves long walks on the beach and playing at dog parks.

## Odin

This one is Odin:

![odin](/images/odin.resized.jpg)

He is still a baby pup, but aspires to be as great as Atticus one day. 