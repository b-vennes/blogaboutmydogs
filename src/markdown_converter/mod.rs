use std::fs::read_to_string;
use comrak::{markdown_to_html, ComrakOptions};

pub fn convert_md_to_html(path_to_md: String) -> Result<String, std::io::Error> {
    let md = read_to_string(&path_to_md)?;

    let mut options = ComrakOptions::default();
    options.unsafe_ = true;

    let html = markdown_to_html(&md, &options);

    Ok(html)
}