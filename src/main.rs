#![feature(proc_macro_hygiene, decl_macro)]

#[macro_use] extern crate rocket;
#[macro_use] extern crate serde_derive;
extern crate rocket_contrib;

mod markdown_converter;

use markdown_converter::convert_md_to_html;
use rocket_contrib::templates::{Template};
use rocket::response::status::BadRequest;
use rocket_contrib::serve::StaticFiles;

#[derive(Serialize)]
struct TemplateContext {
    driver: &'static str
}

#[derive(Serialize)]
struct HtmlTemplate {
    html: String
}

#[get("/<file>")]
fn file(file: String) -> Result<Template, BadRequest<String>> {
    let file_path = format!("articles/{}.md", file);
    let test_html_result = convert_md_to_html(file_path);

    match test_html_result {
        Ok(html) => Ok(Template::render("index", &HtmlTemplate {
            html: html
        })),
        Err(e) => Err(BadRequest(Some(format!("{:?}", e))))
    }
}

fn rocket() -> rocket::Rocket {
    rocket::ignite()
        .mount("/", routes![file])
        .mount("/images", StaticFiles::from("images"))
        .attach(Template::fairing())
}

fn main() {
    rocket().launch();
}